package jp.alhinc.morita_yuji.calculate_sales;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

public class CalculateSales {

	public static void main(String[] args) {
		if (args.length != 1 || args.length >= 2) {
			System.out.println("予期せぬエラーが発生しました");
			return;
		}
		HashMap<String, String> branchListMap = new HashMap<String, String>();
		HashMap<String, Long> salesFileMap = new HashMap<String, Long>();

		if(!fileRead(args[0],"branch.lst",branchListMap,salesFileMap,"支店","^[0-9]{3}")){
			return;
		}

		ArrayList<String> fileNameList = new ArrayList();
		File[] files = new File(args[0]).listFiles();

		// 売り上げファイルを一つずつ出力
		for (int i = 0; i < files.length; i++) {
			// 売り上げのファイル名をリストに追加
			if (files[i].getName().matches("^[0-9]{8}.rcd$") && files[i].isFile()) {
				fileNameList.add(files[i].getName());
			}
		}

		//売り上げファイル名が連番になっていない時のエラー処理
		for(int i = 0; i < fileNameList.size()-1; i++) {
			String num = fileNameList.get(i).substring(0,8);
			int nums = Integer.parseInt(num);
			String numb = fileNameList.get(i+1).substring(0,8);
			int numbs = Integer.parseInt(numb);
			if(numbs - nums !=1) {
				System.out.println("売上ファイル名が連番になっていません");
				return;
			}
		}

		// 売り上げファイルの中身をリストに追加
		BufferedReader brs = null;
		for (int i = 0; i < fileNameList.size(); i++) {
			try {
				File filess = new File(args[0], fileNameList.get(i));
				FileReader frs = new FileReader(filess);
				brs = new BufferedReader(frs);
				String line;
				ArrayList<String> salesFileList = new ArrayList();
				while ((line = brs.readLine()) != null) {
					salesFileList.add(line);
				}
				//売り上げファイルの中身が２行以外であった時
				if(salesFileList.size() != 2) {
					System.out.println(fileNameList.get(i) + "のフォーマットが不正です");
					return;
				}
				// 文字出力(仮)
				// System.out.println("1行目 : " + salesFileList.get(0));//支店コード
				// System.out.println("2行目 : " + salesFileList.get(1));//売り上げ金額
				//売り上げ金額のエラー処理
				if(!salesFileList.get(1).matches("^[0-9]+$")){
					System.out.println("予期せぬエラーが発生しました");
					return;
				}
				// 売り上げ金額をlong型に変更
				long money = Long.parseLong(salesFileList.get(1));
				long totalSales = 0;
				//売り上げファイルの支店コードが支店定義ファイルになかった場合のエラー処理
				if(!branchListMap.containsKey(salesFileList.get(0))){
					System.out.println(fileNameList.get(i) + "の支店コードが不正です");
					return;
				}
				// 取り出した金額と同じkeyに該当する金額を加算する処理
				// salesFileMap.get(salesFileList.get(0));
				// 取り出しているだけでどこにも入れていないから意味がない
				totalSales = salesFileMap.get(salesFileList.get(0)) + money;
				//合計金額が10桁を超えたエラー処理
				if(totalSales > 9999999999L){
					System.out.println("合計金額が10桁を超えました");
					return;
				}
				salesFileMap.put(salesFileList.get(0), totalSales);
			} catch (IOException e) {
				System.out.println("予期せぬエラーが発生しました");
				return;
			}
		}

		if(!salesPrint(args[0],"branch.out",branchListMap,salesFileMap)){
			return;
		}

	}

	static boolean fileRead(String path,String fileName, HashMap<String, String> nameMap,HashMap<String, Long> salesFileMap,String definitionName,String characterCheck){
		BufferedReader br = null;
		try {
			File file = new File(path, fileName);
			if (!file.exists()) {
				System.out.println(definitionName + "定義ファイルが存在しません");
				return false;
			}
			FileReader fr = new FileReader(file);
			br = new BufferedReader(fr);
			String line;
			while ((line = br.readLine()) != null) {
				String[] str = line.split(",");
				//支店コードが三桁の数字であった時のエラー処理
				if (!str[0].matches(characterCheck)) {
				System.out.println(definitionName + "定義ファイルのフォーマットが不正です");
				return false;
				}
				//支店定義ファイル一行の要素が１つ以下、３つ以上の場合
				if(str.length != 2){
					System.out.println(definitionName + "定義ファイルのフォーマットが不正です");
					return false;
				}
				nameMap.put(str[0], str[1]);
				salesFileMap.put(str[0], 0L);
			}
		} catch (IOException e) {
			System.out.println("予期せぬエラーが発生しました");
			return false;
		} finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					System.out.println("予期せぬエラーが発生しました");
					return false;
				}
			}
		}
		return true;
	}

	static boolean salesPrint(String path,String fileName, HashMap<String, String> nameMap,HashMap<String, Long> salesFileMap){
		BufferedWriter bw = null;
		try {
			File totalFile = new File(path, fileName);
			FileWriter frs = new FileWriter(totalFile);
			bw = new BufferedWriter(frs);
			for (HashMap.Entry<String, String> sales : nameMap.entrySet()) {
				bw.write(sales.getKey() + "," + sales.getValue() + "," + salesFileMap.get(sales.getKey()));
				bw.newLine();
			}
		} catch (IOException e) {
			System.out.println("予期せぬエラーが発生しました");
			return false;
		} finally {
			if (bw != null) {
				try {
					bw.close();
				} catch (IOException e) {
					System.out.println("予期せぬエラーが発生しました");
					return false;
				}
			}
		}
		return true;
	}
}
